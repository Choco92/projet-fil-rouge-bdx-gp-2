#!/usr/bin/env python
from django.contrib.auth.models import User
from os import getenv

admin_username = getenv('DJANGO_ADMIN_USERNAME', 'admin')
admin_email =  getenv('DJANGO_ADMIN_EMAIL', 'admin@admin.com')
admin_password = getenv('DJANGO_ADMIN_PASSWORD')

User.objects.create_superuser(admin_username, admin_email, admin_password)
