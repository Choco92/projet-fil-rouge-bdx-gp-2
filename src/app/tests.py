from datetime import datetime
from django.test import TestCase
from app.models import Post
from django.utils import timezone

# Create your tests here.
class PostTestCase(TestCase):
  def setUp(self):
    Post.objects.create(date = timezone.now(), photo = "Paysage Montagne")

  def test_photo_is_published(self):
    montagne = Post.objects.get(photo = "Paysage Montagne")
    
    first_value = datetime.timestamp(montagne.date)
    second_value = datetime.timestamp(datetime.now())
    error_message =  "Photo date cannot be in the future"

    self.assertLess(first_value, second_value, error_message)
